load('ImageMaster.mat');
NumsOfVideos = numel(imagemaster);

mkdir('image');

for ii = 1:NumsOfVideos
    
    videoObj = VideoReader(['video\' imagemaster{ii}.root '.avi']);
    num_frames = videoObj.NumberOfFrames;
    
    mkdir(['image\' num2str(ii)]);
    
    fprintf('%d: %s\n',ii,imagemaster{ii}.class);
    
    for jj = 1 : num_frames
        frame = read(videoObj,jj);
        imwrite(frame, ['image\' num2str(ii) '\' num2str(jj) '.jpg']);
    end
end
