Model = 'imagenet-vgg-verydeep-16';
run(fullfile('D:/Toolbox/matconvnet-1.0-beta10', '/matlab/vl_setupnn.m')) ;
net = load(['D:/Toolbox/matconvnet-pretrain-model/' Model]) ;

% Layer Name and Index in the VGG-16 model
layers = {'Conv-4','Conv-5','FC-6', 'FC-7'};
layer_index = [23, 30, 33, 35];

net = vl_simplenn_move(net,'gpu');

load('ImageMaster.mat');
nImgs = [4,10,16,22,28,34,40,46];   %select 8 frames in each video

videoNums = 254;
meanPixel = mean(net.normalization.averageImage,1);
meanPixel = mean(meanPixel, 2);

for kk = 1:4
    mkdir(['Deep_Features/' layers{kk} '/']);
end

for ii = 1:videoNums
    disp(ii);
    hists = cell(numel(nImgs), 4);
    for jj = 1:numel(nImgs)
        
        imageName = ['image/' num2str(ii) '/' num2str(nImgs(jj)) '.bmp'];
        
        im = imread(imageName) ;
        im_ = single(im);
        im_ = bsxfun(@minus, im_, meanPixel);
        
        im_ = gpuArray(im_);
        res = vl_simplenn(net, im_) ;
        
        for kk = 1:4
            hist = res(layer_index(kk)+1).x;
            hist = vl_nnpool(hist,[size(hist,1),size(hist,2)], 'method', 'avg');
            hist = squeeze(gather(hist));
            
            hists{jj, kk} = snorm(hist);
        end
    end
    
    for kk = 1:4
        hist = cat(2,hists{:,kk});
        save(['Deep_Features/' layers{kk} '/' num2str(ii) '.mat'],'hist');
    end
end


